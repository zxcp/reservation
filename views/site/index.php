<?php

/* @var $this yii\web\View */
/* @var array $roomsByType */
/* @var DatesForm $model */

use app\models\forms\DatesForm;
use app\models\Room;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Reservation';
?>
<div class="site-index">
    <div class="jumbotron text-center bg-transparent">
        <img width="500px" src="img/orbita.png" alt="Гостиница Орбита">
    </div>

    <p>Выберите даты бронирования, чтобы узнать количество свободных номеров:</p>

    <div class="alert alert-primary">
        <?php $form = ActiveForm::begin([
            'id' => 'dates-form',
            'layout' => 'horizontal',
            'options' => [
                'class' => 'row',
            ],
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-8 d-inline-block\">{input}</div>",
                'labelOptions' => ['class' => 'col-lg-4 col-form-label'],
                'options' => [
                    'class' => 'col-lg-5 row'
                ]
            ],
        ]); ?>

            <?= $form->field($model, 'arrivalDate')->textInput(['type' => 'date']); ?>

            <?= $form->field($model, 'dateOfDeparture')->textInput(['type' => 'date']); ?>

            <div class="offset-1 col-lg-1">
                <?= Html::submitButton('Найти', ['class' => 'btn btn-primary']) ?>
            </div>

        <?php ActiveForm::end(); ?>
    </div>

    <div class="body-content">
        <?php foreach ($roomsByType as $type => $rooms): ?>
            <?php /** @var Room $room */ ?>
            <?php $room = $rooms[0] ?>
            <div class="row mb-4">
                <div class="col-12">
                    <div class="card text-end" style="text-align: right">
                        <div class="row g-0">
                            <div class="col-md-4">
                                <img src="img\<?= $type ?>.jpg" class="card-img" alt="<?= $room->roomType->type ?>">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title"><?= $room->roomType->type ?> (<?= count($rooms) ?>)</h5>
                                    <p class="card-text"><?= $room->roomType->description ?></p>
                                    <a href="<?= Url::to('?r=site/reservation'
                                            . '&type=' . $type
                                            . '&from=' . $model->arrivalDate
                                            . '&to=' . $model->dateOfDeparture
                                        ) ?>"
                                       class="btn btn-primary"
                                    >Забронировать</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
