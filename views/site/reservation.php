<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model app\models\forms\ReservationForm */
/* @var $numbers int[] */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

$this->title = 'Бронирование';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <img class="mb-4 room-img" src="img/<?= $model->type ?>.jpg" alt="">

    <?php $form = ActiveForm::begin([
        'id' => 'reservation-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-2 col-form-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'value' => $model->name]) ?>

    <?= $form->field($model, 'roomId')->dropdownList($numbers) ?>

    <?= $form->field($model, 'arrivalDate')->textInput([
        'type' => 'date',
        'value' => $model->arrivalDate,
        'readonly' => true
    ]); ?>

    <?= $form->field($model, 'dateOfDeparture')->textInput([
        'type' => 'date',
        'value' => $model->dateOfDeparture,
        'readonly' => true
    ]) ?>

    <div class="form-group">
        <div class="offset-lg-1 col-lg-11">
            <?= Html::submitButton('Бронирование', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>