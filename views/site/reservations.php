<?php

/* @var $this yii\web\View */
/* @var $userReservations \app\models\Reservation[] */

use yii\bootstrap4\Html;
use yii\helpers\Url;

$this->title = 'Ваша бронь';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php if ($userReservations): ?>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Имя</th>
                    <th scope="col">Номер</th>
                    <th scope="col">Дата заезда</th>
                    <th scope="col">Дата выезда</th>
                    <th scope="col">Действия</th>
                </tr>
            </thead>
            <tbody>
                <? foreach ($userReservations as $reservation): ?>
                    <tr>
                        <th scope="row"><?= $reservation->id ?></th>
                        <td><?= $reservation->name ?></td>
                        <td><?= $reservation->number ?></td>
                        <td><?= (new DateTime($reservation->arrival_date))->format('d.m.Y H:i:s') ?></td>
                        <td><?= (new DateTime($reservation->date_of_departure))->format('d.m.Y H:i:s') ?></td>
                        <td>
                            <a class="btn btn-danger" href="<?= Url::to('?r=site/delete&id=' . $reservation->id) ?>">
                                Удалить
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <h4>У вас пока нет брони</h4>
    <?php endif; ?>
</div>
