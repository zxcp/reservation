<?php

namespace app\classes;

class Helper
{
    /**
     * Выводит данные на экран
     * @param $data
     * @param bool $exit
     */
    public static function dd($data, bool $exit = true)
    {
        switch (true) {
            case is_array($data):
            case is_object($data):
                echo '<pre>';
                print_r($data);
                echo '</pre>';
                break;
            default:
                var_dump($data);
                break;
        }
        if ($exit) {
            exit;
        }
    }
}
