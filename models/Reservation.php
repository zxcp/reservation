<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class Reservation
 * @package app\models
 * @property int $id
 * @property string $name
 * @property int $room_id
 * @property int $number
 * @property int $arrival_date
 * @property int $date_of_departure
 */
class Reservation extends ActiveRecord
{
    public static function tableName()
    {
        return '{{reservations}}';
    }
}
