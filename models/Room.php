<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class Room
 * @package app\models
 * @property int $id
 * @property int $type
 * @property int $number
 * @property Reservation[] $reservations
 * @property RoomType $roomType
 */
class Room extends ActiveRecord
{
    public static function tableName(): string
    {
        return '{{rooms}}';
    }

    public function getReservations()
    {
        return $this->hasMany(Reservation::class, ['room_id' => 'id']);
    }

    public function getRoomType()
    {
        return $this->hasOne(RoomType::class, ['id' => 'type']);
    }

    /**
     * @param string $from
     * @param string $to
     * @return bool
     */
    public function isReserved(string $from, string $to)
    {
        return (bool) self::getFreeRooms($from, $to, null, $this->id);
    }

    /**
     * Возвращает свободные комнаты для временного интервала от $from до $to
     *
     * @param string $from 'Y-m-d'
     * @param string $to
     * @param int|null $type
     * @param int|null $roomId
     * @return Room[]
     */
    public static function getFreeRooms(string $from, string $to, int $type = null, int $roomId = null)
    {
        $reservationQuery = Reservation::find()
            ->select('room_id')
            ->where([
                //$from <= arrival_date <= $to
                'OR', ['AND', ['>=', 'arrival_date', $from], ['<=', 'arrival_date', $to]], [
                    //$from <= date_of_departure <= $to
                    'OR', ['AND', ['>=', 'date_of_departure', $from], ['<=', 'date_of_departure', $to]], [
                        'OR', [//arrival_date <= $from AND date_of_departure >= $to)
                            'AND',
                            ['<=', 'arrival_date', $from],
                            ['>=', 'date_of_departure', $to]
                        ], [
                            //'arrival_date' <= $from <= 'date_of_departure'
                            'OR', ['AND', ['<=', 'arrival_date', $from], ['>=', 'date_of_departure', $from]], [
                                //arrival_date <= $to <= date_of_departure
                                'OR', ['AND', ['<=', 'arrival_date', $to], ['>=', 'date_of_departure', $to]], [
                                ], [//$from <= arrival_date AND $to >= date_of_departure)
                                    'AND',
                                    ['>=', 'arrival_date', $from],
                                    ['<=', 'date_of_departure', $to]
                                ]
                            ]
                        ]
                    ]
                ]
            ]);

        $roomQuery = Room::find()->where(['NOT IN', 'id', $reservationQuery]);
        if ($type) {
            $roomQuery->andWhere(['rooms.type' => $type]);
        }
        if ($roomId) {
            $roomQuery->andWhere(['rooms.id' => $roomId]);
            $reservationQuery->andWhere(['room_id' => $roomId]);
        }

        return $roomQuery->all();
    }
}
