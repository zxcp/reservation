<?php

namespace app\models\forms;

use app\classes\Date;
use app\models\Reservation;
use app\models\Room;
use yii\base\Model;

/**
 * Class ReservationForm
 * @package app\models\forms
 * @property string $name
 * @property int $roomId
 * @property int $type
 * @property int $roomNumber
 * @property string $arrivalDate
 * @property string $dateOfDeparture
 */
class ReservationForm extends Model
{
    public string $name = '';
    public int $roomId = 0;
    public int $type = 0;
    public int $roomNumber = 0;
    public string $arrivalDate = '';
    public string $dateOfDeparture = '';

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'arrivalDate', 'dateOfDeparture'], 'required'],
            [['name'], 'string'],
            [['roomNumber', 'roomId', 'type'], 'integer'],
            [['arrivalDate', 'dateOfDeparture'], 'date', 'format' => 'php:' . Date::DATE_FORMAT]
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'roomId' => 'ID номера',
            'type' => 'Тип номера',
            'roomNumber' => 'Номер',
            'arrivalDate' => 'Дата заезда',
            'dateOfDeparture' => 'Дата выезда'
        ];
    }

    /**
     * Create reservation
     *
     * @return bool
     * @throws \Exception
     */
    public function reservation(): bool
    {
        if ($this->validate()) {
            $room = Room::findOne(['id' => $this->roomId]);
            $reservation = new Reservation();
            $reservation->name = $this->name;
            $reservation->room_id = $room->id;
            $reservation->number = $room->number;
            $reservation->arrival_date = (new \DateTime($this->arrivalDate))->format(Date::DATE_FORMAT . ' 00:00:00');
            $reservation->date_of_departure = (new \DateTime($this->dateOfDeparture))->format(Date::DATE_FORMAT . ' 23:59:59');

            return $reservation->save();
        }

        return false;
    }
}
