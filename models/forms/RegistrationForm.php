<?php

namespace app\models\forms;

use app\classes\traits\GetUserTrait;
use app\models\User;
use yii\base\Model;

/**
 * Class RegistrationForm
 * @package app\models\forms
 * @property string $username
 * @property string $password
 * @property string $email
 */
class RegistrationForm extends Model
{
    use GetUserTrait;

    public string $username = '';
    public string $password = '';
    public string $email = '';

    private ?User $user = null;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username', 'password', 'email'], 'required'],
            [['password'], 'string', 'min' => 4],
            ['email', 'email'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Имя пользователя',
            'password' => 'Пароль',
            'email' => 'E-mail'
        ];
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function register()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->password = \Yii::$app->security->generatePasswordHash($this->password);
            $user->email = $this->email;
            $user->authKey = \Yii::$app->security->generateRandomString();
            $user->save();
            $this->user = $user;

            return true;
        }

        return false;
    }
}
