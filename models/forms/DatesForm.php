<?php

namespace app\models\forms;

use app\classes\Date;
use yii\base\Model;

/**
 * Class DatesForm
 * @package app\models\forms
 * @property string $arrivalDate
 * @property string $dateOfDeparture
 */
class DatesForm extends Model
{
    public string $arrivalDate = '';
    public string $dateOfDeparture = '';

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['arrivalDate', 'dateOfDeparture'], 'required'],
            [['arrivalDate', 'dateOfDeparture'], 'date', 'format' => 'php:' . Date::DATE_FORMAT],
        ];
    }

    public function attributeLabels()
    {
        return [
            'arrivalDate' => 'Дата заезда',
            'dateOfDeparture' => 'Дата выезда'
        ];
    }
}
