<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class RoomType
 * @package app\models
 * @property int $id
 * @property string $type
 * @property string $description
 */
class RoomType extends ActiveRecord
{
    const TYPE_SINGLE = 1;
    const TYPE_DOUBLE = 2;
    const TYPE_LUX = 3;
    const TYPE_DELUXE = 4;

    public static function tableName(): string
    {
        return '{{room_types}}';
    }
}
