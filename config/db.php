<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=mysql;dbname=reservation',
    'username' => 'user',
    'password' => 'secret',
    'charset' => 'utf8',
];
