<?php

namespace app\controllers;

use app\models\forms\DatesForm;
use app\models\Reservation;
use app\models\forms\ReservationForm;
use app\models\Room;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     * @throws \Exception
     */
    public function actionIndex()
    {
        $model = new DatesForm();
        $rooms = [];
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $rooms = Room::getFreeRooms($model->arrivalDate, $model->dateOfDeparture);
        }

        $roomsByType = [];
        foreach ($rooms as $room) {
            $roomsByType[$room->type][] = $room;
        }

        return $this->render('index', compact('roomsByType', 'model'));
    }

    /**
     * Create reservation
     *
     * @return mixed
     * @throws \Exception
     */
    public function actionReservation()
    {
        $model = new ReservationForm();

        //GET
        if (Yii::$app->request->isGet) {
            $model->type = (int)Yii::$app->request->get('type');
            $model->arrivalDate = (string)Yii::$app->request->get('from');
            $model->dateOfDeparture = (string)Yii::$app->request->get('to');
            $model->name = Yii::$app->user->isGuest ? 'Guest' : Yii::$app->user->identity->username;

            $numbers = ArrayHelper::map(
                Room::getFreeRooms($model->arrivalDate, $model->dateOfDeparture, $model->type),
                'id',
                'number'
            );

            return $this->render('reservation', compact('model', 'numbers'));
        }

        //POST
        if (
            Yii::$app->request->isPost
            && $model->load(Yii::$app->request->post())
            && $model->reservation()
        ) {
            $this->goHome();
        }

        return $this->goBack();
    }

    /**
     * Reservation list
     *
     * @return string
     */
    public function actionReservations()
    {
        $userReservations = Reservation::findAll(['name' => Yii::$app->user->identity->username]);

        return $this->render('reservations', compact('userReservations'));
    }

    /**
     * Delete reservation
     *
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete()
    {
        $reservationId = (int)Yii::$app->request->get('id');
        $reservation = Reservation::findOne([
            'id' => $reservationId,
            'name' => Yii::$app->user->identity->username
        ]);
        if ($reservation) {
            $reservation->delete();
        }

        $this->redirect('?r=site/reservations');
    }
}
