<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m211017_105238_reservations_table
 */
class m211017_105238_reservations_table extends Migration
{
    const TABLE = 'reservations';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING,
            'room_id' => Schema::TYPE_INTEGER,
            'number' => Schema::TYPE_INTEGER,
            'arrival_date' => $this->timestamp()->null(),
            'date_of_departure' => $this->timestamp()->null(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey(
            'fk-rooms',
            self::TABLE,
            'room',
            'rooms',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }
}
