<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m211017_133701_users_table
 */
class m211017_133701_users_table extends Migration
{
    const TABLE = 'users';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => Schema::TYPE_PK,
            'username' => Schema::TYPE_STRING,
            'email' => Schema::TYPE_STRING,
            'password' => Schema::TYPE_STRING,
            'authKey' => Schema::TYPE_STRING
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }
}
