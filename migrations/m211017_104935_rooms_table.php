<?php

use app\models\RoomType;
use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m211017_104935_rooms_table
 */
class m211017_104935_rooms_table extends Migration
{
    const TABLE = 'rooms';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => Schema::TYPE_PK,
            'type' => Schema::TYPE_INTEGER,
            'number' => Schema::TYPE_INTEGER
        ]);

        $this->addForeignKey(
            'fk-room-types',
            self::TABLE,
            'type',
            'room_types',
            'id',
            'CASCADE'
        );

        $this->insert(self::TABLE, [
            'type' => RoomType::TYPE_SINGLE,
            'number' => 201
        ]);
        $this->insert(self::TABLE, [
            'type' => RoomType::TYPE_SINGLE,
            'number' => 202
        ]);
        $this->insert(self::TABLE, [
            'type' => RoomType::TYPE_DOUBLE,
            'number' => 301
        ]);
        $this->insert(self::TABLE, [
            'type' => RoomType::TYPE_DOUBLE,
            'number' => 302
        ]);
        $this->insert(self::TABLE, [
            'type' => RoomType::TYPE_DOUBLE,
            'number' => 303
        ]);
        $this->insert(self::TABLE, [
            'type' => RoomType::TYPE_DOUBLE,
            'number' => 304
        ]);
        $this->insert(self::TABLE, [
            'type' => RoomType::TYPE_LUX,
            'number' => 401
        ]);
        $this->insert(self::TABLE, [
            'type' => RoomType::TYPE_LUX,
            'number' => 402
        ]);
        $this->insert(self::TABLE, [
            'type' => RoomType::TYPE_LUX,
            'number' => 403
        ]);
        $this->insert(self::TABLE, [
            'type' => RoomType::TYPE_DELUXE,
            'number' => 501
        ]);
        $this->insert(self::TABLE, [
            'type' => RoomType::TYPE_DELUXE,
            'number' => 502
        ]);
        $this->insert(self::TABLE, [
            'type' => RoomType::TYPE_DELUXE,
            'number' => 503
        ]);
        $this->insert(self::TABLE, [
            'type' => RoomType::TYPE_DELUXE,
            'number' => 504
        ]);
        $this->insert(self::TABLE, [
            'type' => RoomType::TYPE_DELUXE,
            'number' => 505
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }
}
