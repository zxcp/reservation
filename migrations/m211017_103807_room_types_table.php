<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m211017_103807_test
 */
class m211017_103807_room_types_table extends Migration
{
    const TABLE = 'room_types';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => Schema::TYPE_PK,
            'type' => Schema::TYPE_STRING,
            'description' => Schema::TYPE_STRING,
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');


        $this->insert(self::TABLE, [
            'type' => 'Одноместный',
            'description' => 'Небольшие,уютные однокомнатные номера прекрасно подойдут для тех, кто путешествует в одиночку и не хочет платить за лишнее пространство.'
        ]);
        $this->insert(self::TABLE, [
            'type' => 'Двуместный',
            'description' => 'Компактные и уютные номера прекрасно подойдут для тех гостей, которые не хотят платить за лишнее пространство. Площадь номера-17 м².'
        ]);
        $this->insert(self::TABLE, [
            'type' => 'Люкс',
            'description' => 'Уютные номера категории "Люкс" идеально подойдут не только для семейного размещения, а так же для людей, которые ценят свой статус и комфорт.'
        ]);
        $this->insert(self::TABLE, [
            'type' => 'Де-Люкc',
            'description' => 'Это уникальный двухуровневый номер апартамент расположен на 14 этаже нашей гостиницы, откуда откроется чарующий вид на город.'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }
}
